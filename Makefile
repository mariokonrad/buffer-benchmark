.PHONY: default clean prepare build run

default : build run

clean :
	rm -fr build

prepare : clean
	cmake -B build/deps -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_COMPILER=g++-9 deps
	cmake --build build/deps -j 4

build :
	cmake -B build -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_COMPILER=g++-9  .
	cmake --build build -j 4

run :
	build/benchmark

