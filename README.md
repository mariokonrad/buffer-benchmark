# README buffer-benchmark

Code contained in this repository is distributed under _public domain_.

## Introduction

This is a basic microbenchmark for buffer handling (init, copy) in C and C++.

It uses Goole benchmark framework, also GNU make and CMake are involved.


## Build and Run

Build prequisites:

	$ make prepare
	$ make build
	$ make run


## Compilers

In order to use other compilers to build and run the benchmarks, the
file `Makefile` needs to be modified accordingly.

The benchmark was tested with GCC-9 only.


