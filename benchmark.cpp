#include <benchmark/benchmark.h>
#include <algorithm>
#include <cstring>
#include <memory>
#include <numeric>
#include <vector>


void simple(std::unique_ptr<uint8_t[]> & data,
            std::size_t,
            std::size_t buf_size,
            const std::vector<std::vector<int16_t>> & buffers)
{
	std::size_t i = 0u;
	for (auto && b : buffers)
		std::copy(begin(b), end(b), data.get() + buf_size * sizeof(int16_t) * i++);
}

void bench_simple(benchmark::State & state)
{
	const std::size_t num_buf  = state.range(0);
	const std::size_t buf_size = state.range(1);

	// prepare buffers
	using buffer = std::vector<int16_t>;
	std::vector<buffer> buffers;

	buffers.resize(num_buf);
	for (auto && b : buffers)
		b.resize(buf_size);

	// fill buffers with "random" data
	for (auto && b : buffers)
		std::iota(begin(b), end(b), 1);

	// allocate output buffer
	auto data = std::make_unique<uint8_t[]>(num_buf * buf_size * sizeof(int16_t));

	for (auto _ : state) {
		simple(data, num_buf, buf_size, buffers);
		benchmark::DoNotOptimize(data);
	}
}

BENCHMARK(bench_simple)
	->Args({10,  128})
	->Args({10,  256})
	->Args({10,  512})
	->Args({10,  960})
	->Args({10, 1024})
	->Args({10, 2048})
	;


template <std::size_t num_buf, std::size_t buf_size>
void simple_templated(std::unique_ptr<uint8_t[]> & data,
                      const std::array<std::array<int16_t, buf_size>, num_buf> & buffers)
{
	std::size_t i = 0u;
	for (const auto & b : buffers)
		std::copy(begin(b), end(b), data.get() + buf_size * sizeof(int16_t) * i++);
}

template <std::size_t num_buf, std::size_t buf_size>
void bench_simple_templated(benchmark::State & state)
{
	// prepare buffers
	using buffer = std::array<int16_t, buf_size>;
	std::array<buffer, num_buf> buffers;

	// fill buffers with "random" data
	for (auto && b : buffers)
		std::iota(begin(b), end(b), 1);

	// prepare output data
	auto data = std::make_unique<uint8_t[]>(num_buf * buf_size * sizeof(int16_t));

	for (auto _ : state) {
		simple_templated<num_buf, buf_size>(data, buffers);
		benchmark::DoNotOptimize(data);
	}
}

BENCHMARK_TEMPLATE(bench_simple_templated, 10,  128);
BENCHMARK_TEMPLATE(bench_simple_templated, 10,  256);
BENCHMARK_TEMPLATE(bench_simple_templated, 10,  512);
BENCHMARK_TEMPLATE(bench_simple_templated, 10,  960);
BENCHMARK_TEMPLATE(bench_simple_templated, 10, 1024);
BENCHMARK_TEMPLATE(bench_simple_templated, 10, 2048);

void c_style(uint8_t * data,
             std::size_t num_buf,
             std::size_t buf_size,
             int16_t ** buffers)
{
	for (std::size_t i = 0u; i < num_buf; ++i)
		std::memcpy(data + i * buf_size * sizeof(int16_t), &buffers[i][0], buf_size * sizeof(int16_t));
}

void bench_c_style(benchmark::State & state)
{
	const std::size_t num_buf  = state.range(0);
	const std::size_t buf_size = state.range(1);

	// prepare buffers
	int16_t ** buffers = new int16_t* [num_buf];
	for (std::size_t i = 0u; i < num_buf; ++i)
		buffers[i] = new int16_t[buf_size];

	// fill buffers with "random" data
	for (std::size_t i = 0u; i < num_buf; ++i)
		std::iota(&buffers[i][0], &buffers[i][buf_size], 1);

	// prepare output buffer
	uint8_t * data = new uint8_t[num_buf * buf_size * sizeof(int16_t)];

	for (auto _ : state) {
		c_style(data, num_buf, buf_size, buffers);
		benchmark::DoNotOptimize(data);
	}

	// free resources
	for (std::size_t i = 0u; i < num_buf; ++i)
		delete [] buffers[i];
	delete [] buffers;
}

BENCHMARK(bench_c_style)
	->Args({10,  128})
	->Args({10,  256})
	->Args({10,  512})
	->Args({10,  960})
	->Args({10, 1024})
	->Args({10, 2048})
	;


void c_style_flat(uint8_t * data,
                  std::size_t num_buf,
                  std::size_t buf_size,
                  int16_t * buffers)
{
	for (std::size_t i = 0u; i < num_buf; ++i)
		std::memcpy(data + i * buf_size * sizeof(int16_t), &buffers[i * buf_size], buf_size * sizeof(int16_t));
}

void bench_c_style_flat(benchmark::State & state)
{
	const std::size_t num_buf  = state.range(0);
	const std::size_t buf_size = state.range(1);

	// prepare buffers
	int16_t * buffers = new int16_t [num_buf * buf_size];

	// fill buffers with "random" data
	for (std::size_t i = 0u; i < num_buf; ++i)
		std::iota(&buffers[i * buf_size], &buffers[i * buf_size + buf_size], 1);

	// prepare output buffer
	uint8_t * data = new uint8_t[num_buf * buf_size * sizeof(int16_t)];

	for (auto _ : state) {
		c_style_flat(data, num_buf, buf_size, buffers);
		benchmark::DoNotOptimize(data);
	}

	// free resources
	delete [] buffers;
}

BENCHMARK(bench_c_style_flat)
	->Args({10,  128})
	->Args({10,  256})
	->Args({10,  512})
	->Args({10,  960})
	->Args({10, 1024})
	->Args({10, 2048})
	;

BENCHMARK_MAIN();

